Overview
--------
This module provides a weight field for enabled content types. The default
frontpage is overridden to sort nodes first by sticky, then weight, then created
date. Nodes with a lower weight will be positiioned before those with higher
weights.

Installation
------------
This module can be installed by following the instructions at
https://drupal.org/documentation/install/modules-themes/modules-7

Configuration
-------------
To enable Keyword Rich for a content type, go to Administration > Structure > 
Content types and select the content type you want to enable Keyword Rich for. 
Select the Keyword Rich Settings vertical tab, choose your desired settings, 
and click the Save button.

Set the node header values
--------------------------
To set the header values for a node, go to the node's edit page. Enter values
for the Header and Subheader, then select which one is most keyword rich using
the radio buttons. Then save your node.
