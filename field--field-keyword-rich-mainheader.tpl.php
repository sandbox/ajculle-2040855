<?php

/**
 * @file
 * field--field-keyword-rich-mainheader.tpl.php
 *  This template file renders the output of field_keyword_rich_mainheader
 */

$mostrich = field_get_items('node', $element['#object'], 'field_keyword_rich_mostrich');
$subHeader = field_get_items('node', $element['#object'], 'field_keyword_rich_subheader');
$subValue = field_view_value('node', $element['#object'], 'field_keyword_rich_subheader', $subHeader[0]);
$output = (isset($mostrich[0]['value']) && $mostrich[0]['value'] == 'subheader') ? ('<hgroup class="keyword-rich"><h2 class="field-keyword-rich-mainheader">' . html_entity_decode(render($items[0])) . '</h2>') : ('<hgroup><h1 class="field-keyword-rich-mainheader">' . html_entity_decode(render($items[0])) . '</h1>');
$output .= ($subValue['#markup'] == '' || $subValue['#markup'] == NULL) ? "</hgroup>" : "";
echo ($output);

?>
