<?php

/**
 * @file
 * field--field-keyword-rich-subheader.tpl.php
 *  This template file renders the output of field_keyword_rich_subheader
 */

$mostrich = field_get_items('node', $element['#object'], 'field_keyword_rich_mostrich');
$output = (isset($mostrich[0]['value']) && $mostrich[0]['value'] == 'subheader') ? ('<h1 class="field-keyword-rich-subheader">' . html_entity_decode(render($items[0])) . '</h1></hgroup>') : ('<h2 class="field-keyword-rich-subheader">' . html_entity_decode(render($items[0])) . '</h2></hgroup>');
echo ($output);

?>
